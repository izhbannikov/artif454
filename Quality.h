/* 
 * File:   Quality.h
 * Author: ilya
 *
 * Created on 9 Август 2012 г., 14:33
 */

#ifndef QUALITY_H
#define	QUALITY_H

#include <stdio.h>
#include <iostream>
#include <exception>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <streambuf>
#include <math.h>
#include <list>
#include "Read.h"

class Quality {
public:
    Quality();
    Quality(const Quality& orig);
    virtual ~Quality();
    
    /*Custom methods*/
    float RevQPhred(char q);
    float RevQSolexa(char q);
    
private:
    

};

#endif	/* QUALITY_H */

