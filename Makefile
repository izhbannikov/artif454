CXX = g++
CFLAGS = -Wall -g -Xlinker -zmuldefs


all:  util.o Error.o Quality.o main.o artif454 
		
					
artif454 :  util.o Error.o Quality.o  main.o
	$(CXX) $(CFLAGS)  -o  artif454 main.o Quality.o Error.o util.o 
	
main.o :  
	$(CXX) $(CFLAGS)  -c main.cpp
	
Quality.o :  
	$(CXX) $(CFLAGS)  -c Quality.cpp
	
Error.o :  
	$(CXX) $(CFLAGS)  -c Error.cpp
	
util.o :
	$(CXX) $(CFLAGS)  -c util.cpp


clean:
	rm -rf artif454 *.o
