Usage:
	  ./artif454 -g genome_file
		    [-v vector_file]
                     [ -c file_of_contaminants] 
                     [-o output_file_name] 
                     [-n number_of_reads] 
                     [-vv v_percent ] 
                     [-vg vg_percent] 
                     [-cc c_percent] 
                     [-q qual_file_name] 
                     [-l From To] 
                     [-r Gold_read_len]
                     [-e Erros: yes/no] 
            Where :
                  -g genome_file : Reference genome, required parameter;\n" 
                  -v vector_file : Supplemental file that contains vector genome. If vector is not provided, no reads with vector will be generated;\n" 
                  -c file_of_contaminants : File with contaminant genome. If contaminant(s) is(are) not provided, no reads with contaminants will be generated;\n" 
                  -n number_of_reads: Number of reads to generate;\n" 
                  -vv v_percent : Percentage of reads that contain only vector genome. Note: you have to include supplemental vector file. Default: vv = 0;\n" 
                  -vg vg_percent : Percentage of reads that contain reference genome & vector genome. Note: you have to include supplemental vector file. Default: vg = 100;\n" 
                  -cc c_percent : Percentage of reads that contain contaminants. Note: you have to include supplemental file with contaminants. Default: cc = 0;\n" 
                  -q qual_file_name : Supplemental file for quality assigning;\n" 
                  -l From To : The limits of read lengths. Example: -l 400 800. It will generate reads with length varying from 400 to 800 bases\n" 
                  -r Gold_read_len : The limit (in bases) on read length where right adapter can exist. For those reads with length more than Gold_read_len (default 500 bases), no right adapter will be presented.\n" 
                  -e Erros: yes/no : The error flag. If -e option included, errors will be introduced to artificial reads.\n";
    
    
    Example: ./artif454 -g ../hg19/chr1.fa -v ../vectors.fasta -o ../artif454_10000_0_100_0.fastq -n 10000 -vg 100 -q ../HMAX87Q04.fastq -l 300 600 -r 400\n";

     It generates output file : ../artif454_10000_0_100_0.fastq with 10000 reads, 100% reads contains parts of vector genome, quality scores were taken from supplemental file HMAX87Q04.fastq, with lengths of reads from 300 to 600 bases and with gold_read_length of 400 bases..
