
#include <stdio.h>
#include <iostream>
#include <exception>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <math.h>
#include "timer.h"
#include "util.h"
#include <streambuf>
#include <list>
#include "Error.h"
#include "Read.h"
#include "Quality.h"
#include "Adaptor.h"


using namespace std;

/*Number of artificial 454 reads to generate : */
int NUM_READS = 1000; 

void PrintHelp();
void ReadGen (int num, int v_percent, int vg_percent, int c_percent);
string GenNs(int num, char* letter);
void MakeReport();
void QualGen(char* FileName);

/*This string holds the whole genome :*/
string MainStr;

/*String that holds only vector data :*/
vector<string> VectorStrings;

/*String which holds only contaminants :*/
string ContigStr;


/*Vector that holeds all generated artificial 454 reads : */
vector <ReadInform> ArtifReads;


int GOLD_READ_LEN = 500; /*The lenght of the read when bar code an N-s can exist. */
int V_PERCENT = 0;
int VG_PERCENT = 100;
int C_PERCENT = 0;
bool EFLAG = false; /*Error flag. If it setup to true -> there will be error generating.*/
int FROM = 400;
int TO = 800;
int VECTOR_LEN_AVG = 100;
short ETHRESHOLD = 15;


map<int /*position*/, vector<char> /*quality score*/> QualDict;
map<int, vector<char> >::iterator it_QualDict;

/*
 * Parameters: 
 
 */
int main(int argc, char *argv[]) {
    double start, finish, elapsed;
    
    /*List of files separated by "," : */
    /*Contaminans : */
    char *cont_file_name;
    /*Vectors : */
    char *vector_file_name;
    /*Genome :*/
    char *genome_file;
    
    string output_file_name = "";// = "artif454_10000_0_100_0.fastq";
    
    /*Quality file_name : */
    char* qual_file_name = "../HMAX87Q04.fastq";
    /*******************************************/
    /* Parse command line arguments */
    /*******************************************/
    for (int i=1; i<argc; i++) {
        if (argv[i][0]=='-') { /* an option */
           switch (argv[i][1]) {
              case 'g':
                 genome_file = argv[i+1];
                 break;
              case 'v':
                  if(argv[i][2] == 'v') {
                     V_PERCENT = atoi(argv[i+1]);
                  } else if (argv[i][2] == 'g') {
                     VG_PERCENT = atoi(argv[i+1]);
                  } else if (argv[i][2] == NULL) {
                     vector_file_name = argv[i+1]; 
                  }
                 break;
              case 'c':
                  if(argv[i][2] == 'c') {
                     C_PERCENT = atoi(argv[i+1]); 
                  } else if(argv[i][2] == NULL) {
                     cont_file_name = argv[i+1];
                  }
                 break;
              case 'o':
                 output_file_name = string(argv[i+1]);
                 break;
              case 'n':
                 NUM_READS = atoi(argv[i+1]);
                 break;
              case 'q':
                 qual_file_name = argv[i+1];
                 break;
              case 'l': /*Limits: From - To*/
                 FROM =  atoi(argv[i+1]);atoi(argv[i+1]);
                 TO = atoi(argv[i+2]);
                 break;
              case 'r': /*"Gold read length"*/
                 GOLD_READ_LEN =  atoi(argv[i+1]);
                 break;
              case 'e': /*Error flag*/
                 EFLAG =  true;
                 break;
              case '?':
                 PrintHelp();
                 exit(1);
              case 'h':
                 PrintHelp();
                 exit(1);
              default :
                 cout << "Unknown option: " << argv[i][1] << endl;
                 exit(1);
           }
        } 
    }
    
    if(output_file_name == "") {
        output_file_name = "artif454_N" + string( itoa(NUM_READS, new char[20], 10) ) + "_VC" + string( itoa(V_PERCENT, new char[5], 10) ) + "-" + string( itoa(VG_PERCENT, new char[5], 10) ) + "-" + string( itoa(C_PERCENT, new char[5], 10) ) + "_L" + string( itoa(FROM, new char[5], 10) ) + "-" + string( itoa(TO, new char[5], 10) ) + ".fastq" ;
    }
    
    cout << "Printing parameters: " << endl;
    cout << "genome_file: " << genome_file << endl;
    cout << "vector_file: " << vector_file_name << endl;
    cout << "file_of_contaminants: " << cont_file_name << endl;
    cout << "output_file_name: " << output_file_name << endl;
    cout << "number_of_reads: " << NUM_READS << endl;
    cout << "v_percent: " << V_PERCENT << endl;
    cout << "vg_percent: " << VG_PERCENT << endl;
    cout << "c_percent: " << C_PERCENT << endl;
    cout << "quality_file: " << qual_file_name << endl;
    cout << "FROM: " << FROM << " TO: " << TO << endl;
    cout << "GOLD_READ_LEN: " << GOLD_READ_LEN << endl;
    if(EFLAG == false) cout << "No errors will be introduced" << endl;
    
    
    GET_TIME(start);	
    
    
    std::fstream read_file;
    read_file.open(genome_file);
    std::string line;
    
    /*Making genome string from given genome file :*/
    while ( getline(read_file,line) ) {
       
        if(line.substr(0,1) == ">") continue;
        
        MainStr+= line;
       
    }
    
    read_file.close();
    
    line = "";
    
    read_file.open(vector_file_name);
    /*Making vector string from the given vector file : */
    string VectorStr = "";
    while ( getline(read_file,line) ) {
       
        if(line.substr(0,1) == ">") {
            if(VectorStr != "") {
                VectorStrings.push_back(VectorStr);
                VectorStr.clear();
                continue;
            } else {
                continue;
            }
        }
            
        VectorStr+= line;
       
    }
    
    /*Last add : */
    VectorStrings.push_back(VectorStr);
   
   read_file.close();
   
   line = "";
    
    read_file.open(cont_file_name);
    /*Making vector string from the given vector file : */
    while ( getline(read_file,line) ) {
       
        if(line.substr(0,1) == ">") continue;
        
        ContigStr+= line;
       
    }
    
   read_file.close();
    
   /*Generating the Quality dictionary : */
   QualGen(qual_file_name) ;
   
   
   /*Generate some known number of artificial reads : */
   
    ReadGen(NUM_READS, V_PERCENT, VG_PERCENT, C_PERCENT);
    
      
    /*Making output .fastq file :*/
    FILE* output_file;
    output_file = fopen( output_file_name.c_str(), "w");
    
    for(int k=0; k<ArtifReads.size(); k++) {
        
        fputs(ArtifReads[k].seq_id.c_str(),output_file);/*sequence ID*/
        fputc('\n',output_file);
        fputs(ArtifReads[k].seq.c_str(),output_file); /*Artifitial read*/
        fputc('\n',output_file);
        fputc('+',output_file); /*Symbol "+"*/
        fputc('\n',output_file);
        fputs( ( ArtifReads[k].QualString ).c_str(),output_file); /*Quality*/
        fputc('\n',output_file);
    }
    
    fclose(output_file);
    
    
    
    
    if(EFLAG == true)
        ErrorGen();
    
    MakeReport();
    
    GET_TIME(finish);
    elapsed = finish - start;
    printf("Elapsed time = %e seconds\n", elapsed);
    
    start = finish = elapsed = 0;    
        
    return 0;
}

/*Generates known number of 454 artificial reads : */
/*Parameters :
 * num_reads - total number of reads to generate;
 * v_percent - number of reads which contain only vector
 * vg_percent - number of lines which contain vector + genome
 * c_percent - number of lines which contain contaminants only
 
 
 */
void ReadGen (int num_reads, int v_percent, int vg_percent, int c_percent) {
    
    /*The length of genomic string : */
    int gen_str_len = MainStr.length(); 
    
    /*Generating of the genomic reads (clean reads without any vectors and contaminants) : */
    for(int i=0; i<num_reads; i++) {
        
        /*First lets obtain a length of sequence :*/
        int seq_len = GetRandomInt( FROM, TO );
        /*Second lets choose a random position in the genomic string (MainStr) : */
        int pos = GetRandomInt(0, gen_str_len);//(long)GetRandomDouble(0, gen_str_len);
        
        ReadInform new_454read; /*A structure which hold information about artificial sequence : */ 
        new_454read.c_len = 0;
        new_454read.v_len = 0;
        new_454read.vg_len = 0;
        new_454read.vg_pos = 0;
        new_454read.gv = false; /*vg_len && gv == false means no vector at all*/
        
        new_454read.pos = pos;
        
        if(pos+seq_len > gen_str_len) {
            /*If choosen position + seq_len > then the length of the genomic string : */
            new_454read.seq = MainStr.substr(pos,gen_str_len - pos);
        } else {
            new_454read.seq = MainStr.substr(pos,seq_len);
        }
        
        while(1) {
          size_t found=new_454read.seq.rfind("NN");
          if (found==string::npos) {
             break;
          } else {
             pos = GetRandomInt(0, gen_str_len);
             seq_len = GetRandomInt( FROM, TO );
             
             if(pos+seq_len > gen_str_len) {
                /*If choosen position + seq_len > then the length of the genomic string : */
                new_454read.seq = MainStr.substr(pos,gen_str_len - pos);
             } else {
                new_454read.seq = MainStr.substr(pos,seq_len);
             }
             
             new_454read.pos = pos;
          }
                      
        }
        
        /*Now we have to attach RLMIDS, KEY and a B-adaptor : */
        /*First, randomly choosing a row in the RLMIDS array:*/
        int rlmids_row = GetRandomInt(0, 11);
        
        /*Get and attach left and right clips, bar code and key :*/
        new_454read.seq = KEY + RLMIDS[rlmids_row][0] + new_454read.seq;
        /*Calculating a new lenth of sequence : */
        seq_len = new_454read.seq.length();
        
        /*Assing rclip and lclip : */
        new_454read.rclip = seq_len;
        new_454read.lclip = string(KEY+RLMIDS[rlmids_row][0]).length();
        /*Assigning an A-ADAPTER*/
        new_454read.A_ADAPTER = new_454read.lclip;
        
        
        /*Check if the seq_len <= GOLD_READ_LEN : */
        if(seq_len <= GOLD_READ_LEN) {
            /*Attach a B-adaptor and some N-s or just a part of B-adaptor*/
            if(seq_len + BADAPTER.length() + RLMIDS[rlmids_row][1].length() < GOLD_READ_LEN) {
                
                new_454read.B_ADAPTER = RLMIDS[rlmids_row][1].length() + BADAPTER.length();
                new_454read.rclip = seq_len;
                
                new_454read.seq += RLMIDS[rlmids_row][1] + BADAPTER;
                
                seq_len = new_454read.seq.length();
                
                new_454read.seq += GenNs( GetRandomInt(1, GOLD_READ_LEN - seq_len) , "N" );
                
                
                
            } else {
                new_454read.B_ADAPTER = GOLD_READ_LEN - seq_len;
                new_454read.rclip = seq_len;
                new_454read.seq += RLMIDS[rlmids_row][1] + BADAPTER;
                new_454read.seq = new_454read.seq.substr(0,GOLD_READ_LEN);
                
                seq_len = GOLD_READ_LEN;
            }
        } else {
            new_454read.B_ADAPTER = 0;
            new_454read.rclip = seq_len;
        }
        
        /*Assign status 0 - clean read, no vectors and contaminants : */
        new_454read.status = 0;
        
        /*Generate sequence ID : */
        //new_454read.seq_id = string("@ARTIF") + string(itoa(i, new char[5], 10)); 
        
        /*Save a new artificial sequence in ArtifReads array : */
        ArtifReads.push_back(new_454read);
        
    }
    
    /*Add vector particles (full read contains vector data, i.e. 100% of read is vector data) : */
    /*Calculate the length of vector genome string : */
    /*First, pick up a random index in VectorStrings array: */
    int VStr_index = GetRandomInt(0, VectorStrings.size()-1);
    string VectorStr = VectorStrings[VStr_index];
    int vector_str_len = VectorStr.length();
    if(vector_str_len < TO ) {
       while(1) {
           VStr_index = GetRandomInt(0, VectorStrings.size()-1);
           VectorStr = VectorStrings[VStr_index];
           vector_str_len = VectorStr.length();
           if(vector_str_len >= TO ) break;
       } 
    }
    
    /*Actual number of reads that a supposed to contain 100% vector data : */
    int v_num = ((num_reads)/100)*v_percent;
    
    /*the new "to" :*/
    long lim = ArtifReads.size() - 1;
    /*Finally, genetating the sequences with 100% of vector data : */
    for(int i=0; i<v_num; i++) {
        /*Get the random number as an index in ArtifReads array : */
        int v_index = GetRandomInt(0, lim);
        
        /*Just checking if read is already taken : */
        while(1) {
            if(ArtifReads[v_index].status == 0) {
                break;
            } else {
                v_index = GetRandomInt(0, lim);
            }
        }
        
        
        
        /*The length of the pure genomic sequence string saved in ArtifReads array : */
        int v_string_len = ArtifReads[v_index].rclip - ArtifReads[v_index].lclip;
        /*Get the random position in the vector file*/
        int v_pos = GetRandomInt(0, vector_str_len);
        /*Take a piece of vector string :*/
        string vect_part_str;
        
        int seq_len = ArtifReads[v_index].seq.length();
        
        if(v_pos+v_string_len > vector_str_len) {
           vect_part_str = VectorStr.substr( v_pos, vector_str_len - v_pos );
           seq_len = vector_str_len - v_pos;
        } else {
           vect_part_str = VectorStr.substr( v_pos, v_string_len );
           
        } 
        
        
        /*Replace genome by vector :*/
        
        ArtifReads[v_index].seq = ArtifReads[v_index].seq.substr(0,ArtifReads[v_index].lclip)  +  /*KEY + RL mid*/
                                                                                vect_part_str +  /*vector part*/
                                                                                ArtifReads[v_index].seq.substr(ArtifReads[v_index].rclip, ArtifReads[v_index].seq.length() - ArtifReads[v_index].rclip); /*Tail*/
        
        
        
        ArtifReads[v_index].v_len = vect_part_str.length();
        ArtifReads[v_index].v_seq = vect_part_str;
        
        ArtifReads[v_index].status = 1;
    }
    
    /*Add vector + genome. Read contains some vector data and genome :*/
    /*Patterns: 
      1). V-G-B
      2). G-V-B
     */
    /*First, pick up a random index in VectorStrings array: */
    VStr_index = GetRandomInt(0, VectorStrings.size()-1);
    VectorStr = VectorStrings[VStr_index];
    vector_str_len = VectorStr.length();
    if(vector_str_len < TO ) {
       while(1) {
           VStr_index = GetRandomInt(0, VectorStrings.size()-1);
           VectorStr = VectorStrings[VStr_index];
           vector_str_len = VectorStr.length();
           if(vector_str_len >= TO ) break;
       } 
    }
    
    /*Calculate actual number of reads that contain some vector and genomic data : */
    int vg_num = ((num_reads)/100)*vg_percent;
    lim = ArtifReads.size() - 1;
    /*Making v+g reads :*/
    for(int i=0; i<vg_num; i++) {
        /*Get the random number to serve as an index in ArtifReads array : */
        int vg_index = GetRandomInt(0, lim);
        
        /*Just checking if read is already taken : */
        while(1) {
            if(ArtifReads[vg_index].status == 0) {
                break;
            } else {
                vg_index = GetRandomInt(0, lim);
            }
        }
        
        /*Let us obtain a random length of vector string : */
        int vg_len = GetRandomInt(1,ArtifReads[vg_index].rclip - ArtifReads[vg_index].lclip);
        int vg_pos = 0;
        /*Get the random position in the vector file*/
        //vg_pos = 0;//GetRandomInt(0, vector_str_len);
        /*Take a piece of vector string :*/
        string vg_seq;
        //if(vg_pos+vg_len > vector_str_len) {
        //   vg_seq = VectorStr.substr( vg_pos, vector_str_len - vg_pos );
        //} else {
        //   vg_seq = VectorStr.substr( vg_pos, vg_len );
        //}
        //vg_len = vg_seq.length();
        
        //ArtifReads[vg_index].vg_len = vg_len;
        //ArtifReads[vg_index].vg_seq = vg_seq;
                
        /*V-G-B of G-V-B ? */
        int flag = GetRandomInt(0, 1);
        if(flag == 0) {
            /*V-G-B*/
            /*Get the random position in the vector file*/
            /*Get the random position in the vector file*/
            vg_pos = 0;//GetRandomInt(0, vector_str_len);
            /*Take a piece of vector string :*/
            vg_seq = VectorStr.substr( vg_pos, vg_len );
            
            ArtifReads[vg_index].vg_len = vg_len;
            ArtifReads[vg_index].vg_seq = vg_seq;
            
            ArtifReads[vg_index].vg_pos = ArtifReads[vg_index].lclip;
            
            vg_seq = ArtifReads[vg_index].seq.substr(0,ArtifReads[vg_index].lclip) + vg_seq;  /*vector part*/
            ArtifReads[vg_index].lclip = ArtifReads[vg_index].lclip + vg_len;
            
            vg_len = vg_seq.length();
            ArtifReads[vg_index].seq =  vg_seq + ArtifReads[vg_index].seq.substr(vg_len, ArtifReads[vg_index].seq.length() - vg_len); /*Tail*/
            
            ArtifReads[vg_index].gv = false;
        } else {
            /*G-V-B*/
            vg_pos = vector_str_len - vg_len;//GetRandomInt(0, vector_str_len);
            
            vg_seq = VectorStr.substr( vg_pos, vg_len );
            
            ArtifReads[vg_index].vg_len = vg_len;
            ArtifReads[vg_index].vg_seq = vg_seq;
            
            string tstr = ArtifReads[vg_index].seq.substr(0,ArtifReads[vg_index].rclip - vg_len);
            ArtifReads[vg_index].vg_pos = tstr.length();
            ArtifReads[vg_index].seq = tstr  +  vg_seq   /*vector part*/ +  ArtifReads[vg_index].seq.substr(ArtifReads[vg_index].rclip, ArtifReads[vg_index].seq.length() - ArtifReads[vg_index].rclip); /*Tail*/
            ArtifReads[vg_index].rclip = ArtifReads[vg_index].rclip - vg_len;
            
            ArtifReads[vg_index].gv = true;
        }
        
        
        
        /*Assign status v+g :*/
        ArtifReads[vg_index].status = 2;
    }
    
    /*Add contaminants (like a vector) : */
    int contig_str_len = ContigStr.length();
    int c_num = ((num_reads)/100)*c_percent;
    
    lim = ArtifReads.size() - 1;
    for(int i=0; i<c_num; i++) {
        /*Get the random number to serve as an index in ArtifReads array : */
        int c_index = GetRandomInt(0, lim);
        
        while(1) {
            if(ArtifReads[c_index].status == 0) {
                break;
            } else {
                c_index = GetRandomInt(0, lim);
            }
        }
        
        /*The length of the vector string : */
        int c_string_len = ArtifReads[c_index].rclip - ArtifReads[c_index].lclip;
        /*Get the random position in the vector file*/
        int c_pos = GetRandomInt(0, contig_str_len - c_string_len);
        /*Take a piece of contig string :*/
        string contig_part_str = ContigStr.substr(c_pos,c_string_len);
        
        ArtifReads[c_index].c_len = c_string_len;
        ArtifReads[c_index].c_seq = contig_part_str;
        
        
        /*Replace genome by contaminant :*/
        ArtifReads[c_index].seq = ArtifReads[c_index].seq.substr(0,ArtifReads[c_index].lclip)  +  /*KEY + RL mid*/
                                                                                contig_part_str +  /*contig part*/
                                                                                ArtifReads[c_index].seq.substr(ArtifReads[c_index].rclip, ArtifReads[c_index].seq.length() - ArtifReads[c_index].rclip); /*RL mid + barcode*/
        /*Assign status of contig :*/
        ArtifReads[c_index].status = 3;
    }
    
    
    /*Making complements (50/50 chance) :*/
    //lim = ArtifReads.size() - 1;
    for(int j=0; j<ArtifReads.size(); j++) {
        if(GetRandomInt(0, 1) == 1) {
            /*Let us make a complement: */
            ArtifReads[j].seq = /*LClip*/ ArtifReads[j].seq.substr( 0, ArtifReads[j].lclip ) + 
                                /*Sequence complement*/ MakeSeqComplement( ArtifReads[j].seq.substr( ArtifReads[j].lclip, ArtifReads[j].rclip - ArtifReads[j].lclip ) ) + 
                                /*The rest of the sequence*/ ArtifReads[j].seq.substr( ArtifReads[j].rclip, ArtifReads[j].seq.length() - ArtifReads[j].rclip ) ;
        }
    }
    
    
    
    /*Now put quality string for each artificial read : */
    string QualStr = "";
    for(int j=0; j<ArtifReads.size(); j++) {
        int read_len = ArtifReads[j].seq.length();
        
        /*Get a read*/ 
        /*For each symbol in the read find appropriate record in the QualDictionary : */
        for(int k=0; k< read_len; k++) {
            
            if(ArtifReads[j].seq[k] == 'N') {
                QualStr += '!';
            } else {
                
                it_QualDict = QualDict.find(k);
                int qual_pos = GetRandomInt(0,(*it_QualDict).second.size());
                
                if((*it_QualDict).second[qual_pos] == NULL) {
                    while(1) {
                       qual_pos = GetRandomInt(0,(*it_QualDict).second.size());
                       if( (*it_QualDict).second[qual_pos] != NULL ) {
                           break;
                       }
                    }
                }
                
                QualStr += (*it_QualDict).second[qual_pos];
            }
            
        }
        
        ArtifReads[j].QualString = QualStr;
        
        QualStr.clear();
    }
    
    /*Generate sequence ID : */
    for(int j=0; j<ArtifReads.size(); j++) {
        
        if(ArtifReads[j].gv == false) {
            switch(ArtifReads[j].status) {
                case 0 : ArtifReads[j].READ = ArtifReads[j].rclip - ArtifReads[j].lclip;
                break;
                case 1 : ArtifReads[j].READ = 0;
                case 3 : ArtifReads[j].READ = 0;
                break;
                case 2 : ArtifReads[j].READ = ArtifReads[j].rclip - ArtifReads[j].vg_pos - ArtifReads[j].vg_len;
                break;
            }
            
        } else {
            ArtifReads[j].READ = ArtifReads[j].vg_pos - ArtifReads[j].lclip ;
        }
        
        
        string seq_tail = "";
        
        switch ( ArtifReads[j].status ) {
            /*Clean vectors, no contaminants & vectors*/
            case 0 : seq_tail = "R" + string(itoa(ArtifReads[j].READ, new char[5], 10))  + /*Right midtag*/ "B" + string(itoa(ArtifReads[j].B_ADAPTER, new char[5], 10));
            break;
            /*Whole read is a vector*/
            case 1 : seq_tail = "V" + string(itoa(ArtifReads[j].v_len, new char[5], 10)) + /*Right midtag*/ "B" + string(itoa(ArtifReads[j].B_ADAPTER, new char[5], 10));
            break;
            /*Genome & part of Vector*/
            case 2 : ArtifReads[j].gv == true ? 
                        seq_tail = "R" + string(itoa(ArtifReads[j].READ, new char[5], 10))  + 
                        /*Vector*/ "V" + string(itoa(ArtifReads[j].vg_len, new char[5], 10)) +
                  /*Right midtag*/ "B" + string(itoa(ArtifReads[j].B_ADAPTER, new char[5], 10)) :
                        /*Vector*/ "V" + string(itoa(ArtifReads[j].vg_len, new char[5], 10)) + 
                                   "R" + string(itoa(ArtifReads[j].READ, new char[5], 10))  + 
                  /*Right midtag*/ "B" + string(itoa(ArtifReads[j].B_ADAPTER, new char[5], 10)) ;
            break;
            case 3 :  "C" + string(itoa(ArtifReads[j].c_len, new char[5], 10)) + /*Right midtag*/ "B" + string(itoa(ArtifReads[j].B_ADAPTER, new char[5], 10));
            break;
            /*Clean vectors, no contaminants & vectors*/
            default : "R" + string(itoa(ArtifReads[j].READ, new char[5], 10))  + /*Right midtag*/ "B" + string(itoa(ArtifReads[j].B_ADAPTER, new char[5], 10));
        } 
        
        ArtifReads[j].seq_id = string("@ARTIF") + string(itoa( j , new char[5], 10)) + "_" +
                                /*Left midtag*/ "A" + string(itoa(ArtifReads[j].A_ADAPTER, new char[5], 10)) + seq_tail ;
        
        seq_tail.clear();
        
    }
   
}


/*HMAX87Q04.fastq*/
void QualGen(char* FileName) {
    
    std::fstream read_file;
    read_file.open(FileName);
    std::string line;
    
    int ii=0;
    vector<string> quality_lines;
    /*Making genome string from given genome file :*/
    while ( getline(read_file,line) ) {
        ii++;
        
        if(ii==4) {
            //quality_lines.push_back(line.substr(15,line.length() - 15));
            quality_lines.push_back(line);
            ii=0;
        }
        
        
    }
    
    read_file.close();
    
    /*Now making a dictionary : */
    for(int i=0; i< quality_lines.size(); i++) {
        /*Take one line : */
        for(int j=0; j<quality_lines[i].length(); j++) {
            
            it_QualDict = QualDict.find(j);
            
            if(it_QualDict == QualDict.end()) {
                vector<char> quality_scores;
                quality_scores.push_back(quality_lines[i][j]);
                QualDict.insert(std::pair<int, vector<char> >(j, quality_scores));
                quality_scores.clear();
            } else {
                (*it_QualDict).second.push_back(quality_lines[i][j]);
            }
            
            
        }
        
        
    }
    
   
    
}

void MakeReport() {
    FILE* rep_file;
    
    
    rep_file = fopen( "artif454_report.csv", "w");
    
    
    //Table header:
    fputs("ReadID,known_lclip,known_rclip,Status,RawLength,v_len, v_seq, vector_len, vector_sequence, vg_pos, c_len, c_seq\n", rep_file );
    
    
    
    for(int ff = 0; ff < ArtifReads.size(); ff++)
    {
        fputs(ArtifReads[ff].seq_id.c_str(), rep_file );
        fputs(",", rep_file );
        fputs(itoa(ArtifReads[ff].lclip,new char[5],10),  rep_file );
        fputs(",", rep_file );
        fputs(itoa(ArtifReads[ff].rclip,new char[5],10),  rep_file );
        fputs(",", rep_file );
        fputs(itoa(ArtifReads[ff].status,new char[5],10),  rep_file );
        fputs(",", rep_file );
        fputs(itoa(ArtifReads[ff].seq.length(),new char[5],10),  rep_file );
        fputs(",", rep_file );
        
        /*Specific information : */
        fputs(itoa(ArtifReads[ff].v_len,new char[5],10),  rep_file );
        fputs(",", rep_file );
        fputs(ArtifReads[ff].v_seq.c_str(),  rep_file );
        fputs(",", rep_file );
        
        fputs(itoa(ArtifReads[ff].vg_len,new char[5],10),  rep_file );
        fputs(",", rep_file );
        fputs(ArtifReads[ff].vg_seq.c_str(),  rep_file );
        fputs(",", rep_file );
        fputs(itoa(ArtifReads[ff].vg_pos,new char[5],10),  rep_file );
        fputs(",", rep_file );
        
        fputs(itoa(ArtifReads[ff].c_len,new char[5],10),  rep_file );
        fputs(",", rep_file );
        fputs(ArtifReads[ff].c_seq.c_str(),  rep_file );
        
        
        
        fputc( '\n', rep_file );
    }
    
    
    fclose(rep_file);
}

string GenNs(int num, char* letter) {
    string nstr;
    
    for(int i=0; i<num; i++) {
        nstr += letter;
    }
    
    return nstr;
}


void PrintHelp() {
    cout << "Usage: \n"; 
    cout << "./artif454 -g genome_file\n"
            "           [-v vector_file]\n" 
            "           [ -c file_of_contaminants]\n" 
            "           [-o output_file_name]\n" 
            "           [-n number_of_reads]\n" 
            "           [-vv v_percent ]\n" 
            "           [-vg vg_percent]\n" 
            "           [-cc c_percent]\n" 
            "           [-q qual_file_name]\n" 
            "           [-l From To]\n" 
            "           [-r Gold_read_len]\n" 
            "           [-e Erros: yes/no]\n" 
            "Where :\n" 
            "      -g genome_file : Reference genome, required parameter;\n" 
            "      -v vector_file : Supplemental file that contains vector genome. If vector is not provided, no reads with vector will be generated;\n" 
            "      -c file_of_contaminants : File with contaminant genome. If contaminant(s) is(are) not provided, no reads with contaminants will be generated;\n" 
            "      -n number_of_reads: Number of reads to generate;\n" 
            "      -vv v_percent : Percentage of reads that contain only vector genome. Note: you have to include supplemental vector file.\n" 
            "      -vg vg_percent : Percentage of reads that contain reference genome & vector genome. Note: you have to include supplemental vector file.\n" 
            "      -cc c_percent : Percentage of reads that contain contaminants. Note: you have to include supplemental file with contaminants;\n" 
            "      -q qual_file_name : Supplemental file for quality assigning;\n" 
            "      -l From To : The limits of read lengths. Example: -l 400 800. It will generate reads with length varying from 400 to 800 bases\n" 
            "      -r Gold_read_len : The limit (in bases) on read length where right adapter can exist. For those reads with length more than Gold_read_len (default 500 bases), no right adapter will be presented.\n" 
            "      -e Erros: yes/no : The error flag. If -e option included, errors will be introduced to artificial reads.\n";
    cout << "Example: ./artif454 -g ../hg19/chr1.fa -v ../vectors.fasta -c ../contaminants.fasta -o ../artif454_10000_0_100_0.fastq -n 10000 -vv 0 -vg 100 -cc 0 -q ../HMAX87Q04.fastq -l 300 600 -r 400\n";
}


void split_str(const string& str, vector<string>& tokens, const string& delimiters = ",")
{
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos = str.find_first_of(delimiters, lastPos);
    
    while (string::npos != pos || string::npos != lastPos)
    {
        cout << str.substr(lastPos, pos - lastPos) << endl;
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
}

