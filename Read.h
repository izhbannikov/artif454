/* 
 * File:   Read.h
 * Author: ilya
 *
 * Created on 7 Август 2012 г., 1:04
 */

#ifndef READ_H
#define	READ_H

#include <stdio.h>
#include <iostream>
#include <string>


using namespace std;


typedef struct {
    string lmid;
    short lmid_id;
    string rmid;
    short rmid_id;
} RL_MID;

typedef struct {
   string readID;
   short discarded;
   int lclip;
   int rclip;
   short contig;
   int initial_length;
   string read;
   string additional_information;
   bool clip_found;
   string fwd_bc;
   string rev_bc;
   RL_MID rlmid;
} Read;

typedef struct {
    string seq_id;
    string seq; /*actual sequence*/
    long pos; /*position in the genome string (in the MainStr)*/
    int lclip;
    int rclip;
    int status; /*0 - genome; 1 - full vector; 2 - vector + genome; 3 - contaminant; 4 - vector + contaminants + genome*/
    int v_len;
    string v_seq;
    int vg_len;
    int vg_pos;
    string vg_seq;
    int c_len;
    string c_seq;
    string QualString;
    int A_ADAPTER;
    int B_ADAPTER;
    int READ; /*between adapters & vector*/
    bool gv; /*GV means: Genome-Vector and TRUE if it is*/
} ReadInform; 

#endif	/* READ_H */

