#include "Error.h"

/*Introduce errors: */
void ErrorGen() {
    Quality *qual = new Quality();
    for(int i=0; i<ArtifReads.size(); i++) {
        
        cout << ArtifReads[i].seq_id << endl;
        
        for(int j = ArtifReads[i].seq.length() - 51; j>=0; j--) {
            
            if( (int)ArtifReads[i].QualString[j] - 33 < 15 ) {
                //cout << "Before: " << ArtifReads[i].seq[j] << " " << qual->RevQPhred(ArtifReads[i].seq[j]) <<  " " << (int)ArtifReads[i].QualString[j] - 33 << " " << ArtifReads[i].QualString[j] << endl;
                ArtifReads[i].seq[j] = MakeBaseComplement( ArtifReads[i].seq[j] );
                //cout << "After: " << ArtifReads[i].seq[j] << " " << qual->RevQPhred(ArtifReads[i].seq[j]) <<  " " << (int)ArtifReads[i].QualString[j] - 33 << " " << ArtifReads[i].QualString[j] << endl;
            }
        }
        
    }
    
    delete qual;
}
