/* 
 * File:   Error.h
 * Author: ilya
 *
 * Created on 9 Август 2012 г., 12:15
 */

#ifndef ERROR_H
#define	ERROR_H

#include <stdio.h>
#include <iostream>
#include <exception>
#include <string>
#include <fstream>
#include <sstream>
#include <map>
#include <vector>
#include <streambuf>
#include <math.h>
#include <list>
#include "Read.h"
#include "Quality.h"
#include "util.h"


extern short ETHRESHOLD;
extern vector <ReadInform> ArtifReads;

void ErrorGen();



#endif	/* ERROR_H */

