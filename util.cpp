#include "util.h"

//create function to calculate matrix maximum score
int get_max(int lucy_clip,  int xml_clip,  int my_clip) {
	int max=0;
	if(lucy_clip > xml_clip) {
            if(lucy_clip >my_clip) {
                max = lucy_clip;
            } else {
                max = my_clip;
            }
	} else {
            if(xml_clip > my_clip) {
                max = xml_clip;
            } else {
                max = my_clip;
            }
        }
	
	return max;
}

int get_min(int lucy_clip,  int xml_clip,  int my_clip) {
	int min=0;
	if(lucy_clip < xml_clip) {
            if(lucy_clip < my_clip) {
                min = lucy_clip;
            } else {
                min = my_clip;
            }
	} else {
            if(xml_clip < my_clip) {
                min = xml_clip;
            } else {
                min = my_clip;
            }
        }
	
	return min;
}

//Convert string to upper case
void stoupper(std::string& s)	{
        std::string::iterator i = s.begin();
        std::string::iterator end = s.end();

        while (i != end) {
                *i = std::toupper((unsigned char)*i);
                ++i;
        }
}

//Split the string based on tokens:
void split(const string& str, vector<string>& tokens, const string& delimiters = " ")
{
    string::size_type lastPos = str.find_first_not_of(delimiters, 0);
    string::size_type pos = str.find_first_of(delimiters, lastPos);
    while (string::npos != pos || string::npos != lastPos)
    {
        tokens.push_back(str.substr(lastPos, pos - lastPos));
        lastPos = str.find_first_not_of(delimiters, pos);
        pos = str.find_first_of(delimiters, lastPos);
    }
}

char* itoa(int value, char* result, int base) {
        // check that the base if valid
	if (base < 2 || base > 36) { *result = '\0'; return result; }
	
        char* ptr = result, *ptr1 = result, tmp_char;
	int tmp_value;
	
	do {
                tmp_value = value;
		value /= base;
		*ptr++ = "zyxwvutsrqponmlkjihgfedcba9876543210123456789abcdefghijklmnopqrstuvwxyz" [35 + (tmp_value - value * base)];
	} while ( value );
	
	// Apply negative sign
	if (tmp_value < 0) *ptr++ = '-';
	*ptr-- = '\0';
	
        while(ptr1 < ptr) {
                tmp_char = *ptr;
		*ptr--= *ptr1;
		*ptr1++ = tmp_char;
	}
	
        return result;
}

string MakeSeqComplement(string init_str) {
    
    for(int i = 0; i<init_str.length(); i++) {
        if(init_str[i] == 'A') {
            init_str[i] = 'T';
            continue;
        }
        if(init_str[i] == 'T') {
            init_str[i] = 'A';
            continue;
        }
        if(init_str[i] == 'G') {
            init_str[i] = 'C';
            continue;
        }
        if(init_str[i] == 'C') {
            init_str[i] = 'G';
            continue;
        }
        
    }
    
    return init_str;
}

char MakeBaseComplement(char base) {
    
    char complement;
    
    /*----------------------------------------*/
    if( base == 'A' ) {
       complement = 'T';
    }
    if( base == 'a' ) {
       complement = 'a';
    }
    /*----------------------------------------*/
    if( base == 'T' ) {
       complement = 'A';
    }
    if( base == 't' ) {
       complement = 'a';
    }
    /*----------------------------------------*/
    if( base == 'G' ) {
       complement = 'C';
       
    }
    if( base == 'g' ) {
       complement = 'c';
       
    }
    /*----------------------------------------*/
    if( base == 'C' ) {
       complement = 'G';
    }
    if( base == 'c' ) {
       complement = 'g';
    }
    /*----------------------------------------*/
        
    
    
    return complement;
}

int GetRandomInt (int from, int to)
{
    
    boost::random::uniform_int_distribution <> dist(from, to);
    
    return dist(gen_int);
}

double GetRandomDouble (double from, double to)
{
    
    boost::random::uniform_real_distribution <> dist(from, to);
    
    return dist(gen_real);
}
