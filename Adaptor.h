/* 
 * File:   Adaptor.h
 * Author: ilya
 *
 * Created on 9 Август 2012 г., 14:35
 */

#ifndef ADAPTOR_H
#define	ADAPTOR_H

#include <stdio.h>
#include <string>

using namespace std;

/*RL mids to attach : */
string RLMIDS[36][2] = {{"ACACGACGACT","AGTCGTGGTGT"}, //RL1{},
                        {"ACACGTAGTAT","ATACTAGGTGT"}, // RL2{}
                  	{"ACACTACTCGT","ACGAGTGGTGT"}, //RL3{}
                        {"ACGACACGTAT","ATACGTGGCGT"}, //RL4{}
                  	{"ACGAGTAGACT","AGTCTACGCGT"}, //RL5{}
                        {"ACGCGTCTAGT","ACTAGAGGCGT"}, //RL6{}
                  	{"ACGTACACACT","AGTGTGTGCGT"}, //RL7{}
                        {"ACGTACTGTGT","ACACAGTGCGT"}, //RL8{}
                  	{"ACGTAGATCGT","ACGATCTGCGT"}, //RL9{}
                        {"ACTACGTCTCT","AGAGACGGAGT"}, //RL10{}
                  	{"ACTATACGAGT","ACTCGTAGAGT"}, //RL11
                        {"ACTCGCGTCGT","ACGACGGGAGT"}, //RL12
                        {"AGACTCGACGT","ACGTCGAGTCT"}, //RL13
                  	{"AGTACGAGAGT",	"ACTCTCGTACT"}, //RL14
                	{"AGTACTACTAT",	"ATAGTAGTACT"}, //RL15
                        {"AGTAGACGTCT",	"AGACGTCTACT"}, //RL16
                        {"AGTCGTACACT",	"AGTGTACGACT"}, //RL17
                        {"AGTGTAGTAGT",	"ACTACTACACT"}, //RL18
                        {"ATAGTATACGT",	"ACGTATACTAT"}, //RL19
                        {"CAGTACGTACT",	"AGTACGTACTG"}, //RL20
                        {"CGACGACGCGT",	"ACGCGTCGTCG"}, //RL21
                        {"CGACGAGTACT",	"AGTACTCGTCG"}, //RL22
                        {"CGATACTACGT",	"ACGTAGTATCG"}, //RL23
                        {"CGTACGTCGAT",	"ATCGACGTACG"}, //RL24
                        {"CTACTCGTAGT",	"ACTACGAGTAG"}, //RL25
                        {"GTACAGTACGT",	"ACGTACTGTAC"}, //RL26
                        {"GTCGTACGTAT",	"ATACGTACGAC"}, //RL27
                        {"GTGTACGACGT",	"ACGTCGTACAC"}, //RL28
                        {"ACACAGTGAGT",	"ACTCACTGTGT"}, //RL29
                        {"ACACTCATACT",	"AGTATGAGTGT"}, //RL30
                        {"ACAGACAGCGT",	"ACGCTGTCTGT"}, //RL31
                        {"ACAGACTATAT",	"ATATAGTCTGT"}, //RL32
                        {"ACAGAGACTCT",	"AGAGTCTCTGT"}, //RL33
                        {"ACAGCTCGTGT",	"ACACGAGCTGT"}, //RL34
                        {"ACAGTGTCGAT",	"ATCGACACTGT"}, //RL35
                        {"ACGAGCGCGCT",	"AGCGCGCTCGT"} //RL36
                        
                       };



string BADAPTER = "GGTCGGCGTCTCTCAAGGCACACAGGGGATAGG";
string KEY = "GACT";




#endif	/* ADAPTER_H */

