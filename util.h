/* 
 * File:   util.h
 * Author: ilya
 *
 * Created on 31 Май 2012 г., 10:27
 */

#ifndef UTIL_H
#define	UTIL_H

#include <stdio.h>
#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#include <boost/random.hpp>
#include <boost/random/mersenne_twister.hpp>
#include <boost/random/uniform_int_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>

#include <ctime>

using namespace std;
using namespace boost;

/*For uniform INT random distribution:*/
boost::random::mt19937 gen_int;

/*For uniform LONG random distribution:*/
boost::random::mt19937 gen_real;

void stoupper(std::string& s);
void split(const string& str, vector<string>& tokens, const string& delimiters);
char* itoa(int value, char* result, int base);
string MakeSeqComplement(string init_str);
int GetRandomInt (int from, int to);
double GetRandomDouble (double from, double to);
char MakeBaseComplement(char base);




#endif	/* UTIL_H */

