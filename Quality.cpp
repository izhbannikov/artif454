/* 
 * File:   Quality.cpp
 * Author: ilya
 * 
 * Created on 9 Август 2012 г., 14:33
 */

#include "Quality.h"

Quality::Quality() {
}

Quality::Quality(const Quality& orig) {
}

Quality::~Quality() {
}

float Quality::RevQPhred(char q) {
    return pow(10,-(q-33)/10);
}


float Quality::RevQSolexa(char q) {
    return pow(10,-(q-33)/10)/( 1+pow(10,-(q-33)/10) ) ;
}

